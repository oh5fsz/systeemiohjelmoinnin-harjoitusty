#include <stdio.h>
#include <sys/wait.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "parser.h"
#include "exec.h"
#include "builtin.h"
int main(void) {
    char    buf[4096];
    struct cmd 	*first;
    struct cmd  *cur;
    // Hostnamen haku nättiä promptia varten.    
    gethostname(hostname, sizeof(hostname));

    // Current working directory.
    curCwd = malloc(sizeof(char*)*1024);

    // Historiatiedosto tallennetaan ajohakemistoon.
    openHistory();

    first = NULL;
    while (1) {
        printf("%s@%s %s $> ", getlogin(), hostname, getcwd(curCwd, 1024));
        while(fgets(buf, sizeof(buf), stdin) != NULL) {
            // Otetaan ikävä newline pois
            if (buf[strlen(buf)-1] == '\n') {
                buf[strlen(buf)-1] = '\0';
            }
            // Kirjoitetaan komento historiaan.
            writeHistory(buf);
            if ((first = commandHandler(buf)) != NULL) {
                
                // Debuggausta.
                cur = first;
                while (cur != NULL) {
                    printf("Commands: %s\n", cur->args[0]);
                    cur = cur->next;
                }
                // Jos komento ei ole shellin sisäinen, palauttaa runBuiltin -1
                // ja komento siirretään execHandlerille.
                if (runBuiltin(first) == -1) {
                    execHandler(first);
                }
            } 
            emptyNodes(first);
            printf("%s@%s %s $> ", getlogin(), hostname, getcwd(curCwd, 1024));          
        }      
    }
    return 0;
}
