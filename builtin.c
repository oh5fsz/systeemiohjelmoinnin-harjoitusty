#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include "parser.h"
#include "builtin.h"
#include "exec.h"

FILE *histfile;

// Tarkistetaan onko annettu komento sisäänrakennettu.
int runBuiltin(struct cmd *first) {
    char	*homedir = getenv("HOME");
    int		retval;

    if (strcmp(first->args[0], "exit") == 0) {
        printf("Quitting!\n");       
        closeHistory();
        emptyNodes(first);         
        free(curCwd);
        exit(0);
    }
    else if (strcmp(first->args[0], "cd") == 0) {
        if (first->args[1] == 0) {
            if (homedir == NULL) {
                printf("Couldn't access home directory. Using HOME=/");
                retval = chdir("/");
            }
            else retval = chdir(homedir);
        }
        else {
            retval = chdir(first->args[1]);
        }
        if (retval != 0) {
            printf("Unable to change directory: %s\n", strerror(errno));
        }
        return 0;
    }
    else if (strcmp(first->args[0], "history") == 0) {
        getHistory();
        return 0;
    }
    else if (strchr(first->args[0], '!')) {
        int linenumber;
        printf("GETHIST: Fetching from history\n");
        sscanf(first->args[0], "!%d", &linenumber);
        if (linenumber == 0) {
            printf("Usage: !linenumber \n");
        }
        else {
            execHistoryCmd(linenumber);
        }
        return 0;
    }
    else return -1;
}

void openHistory() {
    char*    histloc = malloc(sizeof(char*)*1024);
    getcwd(histloc, 1024);
    strcat(histloc, "/history");
    printf("Writing history to: %s\n\n", histloc);
    histfile = fopen(histloc, "a+");
    if (histfile == NULL) {
        printf("History file couldn't be opened: %s", strerror(errno));
    }
    free(histloc);
}

void writeHistory(char* buf) {
    fprintf(histfile, "%s\n", buf);
}

void getHistory() {
    // char c;
    // while((c=fgetc(histfile))!=EOF) {
    //     printf("%c", c);
    // }
    char buf[256];
    rewind(histfile);
    while (fgets(buf, 256, histfile) != NULL) {
        printf("%s", buf);
    }
    fseek(histfile, 0, SEEK_END);
}

void closeHistory() {
    fclose(histfile);
}

void execHistoryCmd(int linenumber) {
    struct cmd* first;
    char buffer[4096];
    int i = 1;
    rewind(histfile);
    
    // Etsitään parametriä vastaava rivinumero.
    for(i=1; i<=linenumber; i++) {
        fgets(buffer, 4096, histfile);
    }
    
    // Poistetaan taas se ikävä newline.
    if (buffer[strlen(buffer)-1] == '\n') {
        buffer[strlen(buffer)-1] = '\0';
    }
    // Palataan historian loppuun.
    fseek(histfile, 0, SEEK_END);
    printf("Got command: %s", buffer);
    first = commandHandler(buffer);
    if (runBuiltin(first) == -1) {
        execHandler(first);
    }
}


// Linkitetyn listan tyhjennys.
void emptyNodes(struct cmd *first) {
    struct cmd *cur;
    struct cmd *tmp;
    int i;
    cur = first;
        while (cur != NULL) {
            tmp = cur->next;
            for (i=0;i<=cur->argc;i++) {
                free(cur->args[i]);
            }
            free(cur->output);
            free(cur->input);
            free(cur);
            cur = tmp;
            if (cur == NULL) break;
        }
}
