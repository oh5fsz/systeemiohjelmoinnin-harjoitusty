#include "parser.h"
#include <stdio.h>
#include <string.h>
#include <glob.h>
#include <stdlib.h>

struct cmd* parseCommand(struct cmd *store, char* command) {
    int	cnt = 1;
    char *tmp;
    // Splitataan argumentit välilyöntien perusteella.
    printf("PARSER: We haz command: %s\n", command);
    tmp = strtok(command, " ");
    store->args[0] = tmp;
    store->background = 0;
    store->input = NULL;
    store->output = NULL;
    // Argumenttejä riittää niin kauan, kunnnes strtok palauttaa NULL:n.
    while (1) {
        tmp = strtok(NULL, " ");
        if (tmp == NULL) {
            break;
        }           
        if (*tmp == '<') {
            command = strtok(NULL, " " );
            store->input = malloc(sizeof(char*)*strlen(command)+1);
            strcpy(store->input, command);
            printf("PARSER: Inputz gonna be redird from: %s\n", store->input);
        }
        else if (*tmp == '>') {
            command = strtok(NULL, " " );
            store->output = malloc(sizeof(char*)*strlen(command)+1);
            strcpy(store->output, command);
            printf("PARSER: Outputz gonna redir to: %s\n", store->output);  
        }
        else if (*tmp == '&') {
            store->background = 1;
            printf("PARSER: Command's gonna be bgrounded\n");
        }
        else {       
            store->args[cnt] = malloc(sizeof(char*)*strlen(tmp)+1);
            strcpy(store->args[cnt], tmp);
            printf("Stored command: %s to slot %i\n", store->args[cnt], cnt);
            cnt = cnt+1;
        }
    }
    printf("PARSER: arguments parsed.\n");       
    store->args[cnt] = NULL;
    store->argc = cnt-1;
    printf("PARSER: command has %i args.\n", store->argc);
    int i;
    // Jos argumenteissa on villikortteja, ajetaan ne globbauksen läpi.
    for(i=0; i<=store->argc; i++) {
        if ((strchr(store->args[i],'*') != NULL) || (strchr(store->args[i], '?') != NULL) || (strchr(store->args[i], '[') != NULL)  || (strchr(store->args[i], '{') != NULL)) {
            printf("PARSER: We has wildcards\n");
            store = findWildcards(store);
        }
    }
    printf("PARSER: After glob, argc is %i\n", store->argc);
    store->args[store->argc+1] = NULL;
    return store;
}

struct cmd* commandHandler(char* buf) {
    struct cmd  *tmp;
    struct cmd  *first;
    struct cmd 	*cur;
    char        *command;
    char        *saveptr;

    first = (struct cmd*)malloc(sizeof(struct cmd));
    first->next = NULL;
    cur = first;
    printf("%s\n", buf);
    // Jos bufferi oli tyhjä, on turha tehdä mitään.
    if (strlen(buf) == 0) {
        return NULL;
    }
    // Tarkistetaan komentoriviargumenteista uudelleenohjaukset ja putket.
    if (strchr(buf, '|')) {
        printf("We haz piping, cyka\n");
        command = strtok_r(buf, "|", &saveptr);
        printf("Next: %s\n", command);
        cur = parseCommand(cur, command);    
        while (command) {
            command = strtok_r(NULL, "|", &saveptr);
            if (command == 0) {
                break;
            }            
            tmp = malloc(sizeof(struct cmd));
            cur->next = tmp;
            tmp->next = NULL;
            tmp = parseCommand(tmp, command);
            cur = tmp;
            printf("HANDLER: Node has command: %s\n", tmp->args[0]);       
        }
    }
    else {
        first = parseCommand(first, buf);
    }
    return first;
}

struct cmd* findWildcards(struct cmd *store) {
    int i;
    glob_t globbuf;
    int globcnt;
    printf("GLOB: argc: %i\n", store->argc);
    // Etsitään villit kortit argumenteista.
    for(i=1; i<=store->argc; i++) {
        // Etsitään globilla villikorttien mukaiset asiat uusiksi argumenteiksi.
        if (globbuf.gl_pathc == 0) {
            printf("GLOB: glob ran once.\n");
            printf("GLOB: glob argument: %s\n", store->args[i]);
            glob(store->args[i], 0, NULL, &globbuf);
        }
        else {
            glob(store->args[i], GLOB_APPEND, NULL, &globbuf);
            printf("GLOB: glob ran again!\n");
            globcnt++;
        }
    }
    store->argc = 0;
    // Poistetaan villikortin sisältävät alkuperäiset argumentit.
    for (i=1; i<globcnt; i++) {
        printf("GLOB: replacing argument: %s\n", store->args[i]);
        printf("GLOB: globbuf.gl_pathc: %zu\n", globbuf.gl_pathc);
        store->args[i] = 0;
    }
    // Vaihdetaan tilalle globilta saadut nimet uusiksi argumenteiksi.
    for (i=0; i<globbuf.gl_pathc; i++) {
        store->args[i+1] = malloc(sizeof(char*)*strlen(globbuf.gl_pathv[i]));
        strcpy(store->args[i+1], globbuf.gl_pathv[i]);
        store->argc = store->argc+1;
    }
    printf("GLOB: glob done\n");
    return store;
}