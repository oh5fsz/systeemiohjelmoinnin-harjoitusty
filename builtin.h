#include <string.h>
#ifndef BUILTIN_H
#define BUILTIN_H
// Tietorakenne ajettavaa komentoa varten.
struct cmd {
	char* args[4096];
	struct cmd* next;
	char* output;
	char* input;
	int argc;
	int background;
};

char    hostname[128];
char	*curCwd;
char    buf[4096];

int runBuiltin(struct cmd *first);
void emptyNodes(struct cmd *first);
void openHistory();
void closeHistory();
void writeHistory();
void getHistory();
void execHistoryCmd();
#endif