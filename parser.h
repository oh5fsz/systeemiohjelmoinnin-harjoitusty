#include "builtin.h"
struct cmd* parseCommand(struct cmd* store, char* command);
struct cmd* commandHandler(char* buf);
struct cmd* findWildcards(struct cmd*);
char* remWhitespaces(char *str);