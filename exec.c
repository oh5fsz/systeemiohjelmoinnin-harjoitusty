#include "builtin.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>

void childReaper(int sig) {
    waitpid(-1, 0, WNOHANG);
}

// Prosessin ajo ilman redirectejä/putkia.
int execCommand(struct cmd *store, int fdIn, int fdOut) {
    if (fdIn != STDIN_FILENO) {
        printf("CHILD: stdin was non-standard\n");
        dup2(fdIn, STDIN_FILENO);
        close(fdIn);
    }
    if (fdOut != STDOUT_FILENO) {
        printf("CHILD: stdout was non-standard\n");
        dup2(fdOut, STDOUT_FILENO);
        close(fdOut);
    }
    execvp(store->args[0], store->args);
    return 0;
}

int execHandler(struct cmd* first) {
    struct cmd* cur;
    int         fdIn = 0;
    int         fdOut = 0;
    int         origSTDIN;
    int         origSTDOUT;
    int         pipefd[2];
    pid_t       child;
    int         state;

    cur = first;
    origSTDOUT = dup(STDOUT_FILENO);
    origSTDIN = dup(STDIN_FILENO);
    while (cur != NULL) {
        if (cur->next) {
            printf("HANDLER: We have multiple commands.\n");
            pipe(pipefd);
            fdOut = pipefd[1];
        }
    else fdOut = STDOUT_FILENO;
        printf("HANDLER: Running command: %s\n", cur->args[0]);
        if (cur->output) {
        printf("HANDLER: We must output to file\n");
            fdOut = open(cur->output, O_WRONLY | O_CREAT,666);
            if (fdOut == -1) {
                printf("Couldn't open %s: %s\n", cur->output, strerror(errno));
                exit(-1);
            }
            printf("Opened file: %s\n", cur->output);  
        }
        if (cur->input) {
            fdIn = open(cur->input, O_RDONLY);
            if (fdIn == -1) {
                printf("Couldn't open %s: %s\n", cur->input, strerror(errno));
                exit(-1);
            printf("Opened file: %s\n", cur->output);                  
            }
        }
        child = fork();
        if (child == 0) {
            execCommand(cur, fdIn, fdOut);
        }
        if (cur->background == 0) {
            child = waitpid(child, &state, 0);
        }
        else if (cur->background == 1) {
            signal(SIGCHLD, childReaper);
        }        

        if (fdIn != STDIN_FILENO) close(fdIn);
        if (fdOut != STDOUT_FILENO) close(fdOut);
        if (cur->next != NULL) {
            cur = cur->next;
            fdIn = pipefd[0];
            printf("HANDLER: We're not the last command. Next command will be: %s\n", cur->args[0]);
        }
        else {
            break;
        }        
    }

    printf("We were the last command.\n");   
    dup2(origSTDIN, STDIN_FILENO);
    printf("HANDLER: Original STDIN restored.\n");
    dup2(origSTDOUT, STDOUT_FILENO);
    printf("HANDLER: Original STDOUT restored.\n");        
    return 0;
}
